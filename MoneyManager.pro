QT += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

SOURCES += \
    main.cpp \
    src/yearmonth.cpp \
    src/event.cpp \
    src/fixedevent.cpp \
    src/periodicevent.cpp \
    src/period.cpp \
    src/eventoperator.cpp \
    src/gui/eventwidget.cpp \
    src/gui/periodwidget.cpp \
    src/budget.cpp \
    src/gui/sidewidget.cpp \
    src/account.cpp \
    src/filehandler.cpp \
    src/gui/mainwindow.cpp \
    src/gui/eventdetailsdialog.cpp \
    src/gui/moneyspinbox.cpp \
    src/periodmanager.cpp \
    src/currentyearmonthloader.cpp \
    src/gui/emittingevent.cpp \
    src/gui/moneylineedit.cpp

HEADERS += \
    src/yearmonth.h \
    src/event.h \
    src/fixedevent.h \
    src/periodicevent.h \
    src/period.h \
    src/eventoperator.h \
    src/common.h \
    src/gui/eventwidget.h \
    src/gui/periodwidget.h \
    src/budget.h \
    src/gui/sidewidget.h \
    src/account.h \
    src/filehandler.h \
    src/gui/mainwindow.h \
    src/gui/eventdetailsdialog.h \
    src/gui/moneyspinbox.h \
    src/periodmanager.h \
    src/currentyearmonthloader.h \
    src/gui/emittingevent.h \
    src/gui/moneylineedit.h

QMAKE_CXXFLAGS += -std=c++11

OTHER_FILES += \
    COPYING
