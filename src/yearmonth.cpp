/*
 * Copyright (C) 2018 Peter Szabo
 *
 * This file is part of MoneyManager.
 *
 * MoneyManager is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MoneyManager is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MoneyManager.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "yearmonth.h"

#include <stdexcept>

using namespace mm;

YearMonth::YearMonth(int year, int month)
{
    if (month < 1 || month > NUM_MONTHS)
    {
        throw std::invalid_argument("month < 1 || month > NUM_MONTHS");
    }

    this->year  = year;
    this->month = month;
}

bool YearMonth::operator==(const YearMonth& other) const
{
    // I know I could just return the expression but I think this boosts readability
    if (year == other.year && month == other.month)
        return true;
    else
        return false;
}

bool YearMonth::operator<(const YearMonth & other) const
{
    if (year < other.year || (year == other.year && month < other.month))
        return true;
    else
        return false;
}

bool YearMonth::operator<=(const YearMonth & other) const
{
    if (year < other.year || (year == other.year && month <= other.month))
        return true;
    else
        return false;
}

bool YearMonth::operator>(const YearMonth & other) const
{
    if (year > other.year || (year == other.year && month > other.month))
        return true;
    else
        return false;
}

bool YearMonth::operator>=(const YearMonth & other) const
{
    if (year > other.year || (year == other.year && month >= other.month))
        return true;
    else
        return false;
}

YearMonth YearMonth::operator+(const int n) const
{
    int plusYears = (month + n) / 12;
    int newMonth  = (month + n) % 12;
    if (newMonth == 0)
    {
        // the 0. month of the year is actually the 12. of the last
        newMonth = 12;
        plusYears -= 1;
    }
    return {year + plusYears, newMonth};
}

int YearMonth::operator-(const YearMonth & other) const
{
    int newYear  = year - other.year;
    int newMonth = month - other.month;
    if (newMonth < 0)
    {
        newYear -= 1;
        newMonth += 12;
    }
    return newYear * 12 + newMonth;
}
