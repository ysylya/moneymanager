/*
 * Copyright (C) 2018 Peter Szabo
 *
 * This file is part of MoneyManager.
 *
 * MoneyManager is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MoneyManager is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MoneyManager.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef PERIODICEVENT_H
#define PERIODICEVENT_H

#include <array>

#include "event.h"
#include "yearmonth.h"
#include "period.h"

namespace mm
{
    class PeriodicEvent : public Event
    {
        std::array<bool, NUM_MONTHS> months;
    public:
        PeriodicEvent() = default;
        PeriodicEvent(std::string description, Money price,
                      std::array<bool, NUM_MONTHS> months,
                      AccountType accountType = AccountType::Cash);
        virtual bool isActiveThatYearMonth(YearMonth yearMonth) override;
        virtual std::string save() override;
        virtual bool load(std::string data) override;
        virtual void finished() override;

        std::array<bool, NUM_MONTHS> getMonths() const;
        void setMonths(const std::array<bool, NUM_MONTHS> & value);
    };
}

#endif // PERIODICEVENT_H
