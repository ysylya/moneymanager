/*
 * Copyright (C) 2018 Peter Szabo
 *
 * This file is part of MoneyManager.
 *
 * MoneyManager is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MoneyManager is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MoneyManager.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "filehandler.h"

using namespace mm;

std::string FileHandler::getPath() const
{
    return path;
}

void FileHandler::setPath(const std::string & value)
{
    path = value;
}

bool FileHandler::openFile(std::fstream & fs, const std::string & name, bool append)
{
    fs.open(name, std::fstream::in | std::fstream::out | (append ? std::fstream::app : std::fstream::trunc));

    if (!fs.is_open())
    {
        // this will create a new file if it doesn't exist,
        // the above "std::fstream::in | std::fstream::out" wouldn't
        std::ofstream ofs {name};

        fs.open(name, std::fstream::in | std::fstream::out | (append ? std::fstream::app : std::fstream::trunc));
        if (!fs.is_open())
            return false;
    }

    return true;
}
