/*
 * Copyright (C) 2018 Peter Szabo
 *
 * This file is part of MoneyManager.
 *
 * MoneyManager is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MoneyManager is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MoneyManager.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QWidget>
#include <string>
#include <memory>

#include "../currentyearmonthloader.h"
#include "../eventoperator.h"
#include "../yearmonth.h"
#include "../periodmanager.h"
#include "../account.h"
#include "periodwidget.h"

namespace mm { namespace gui
{
    class MainWindow : public QWidget
    {
        Q_OBJECT
        CurrentYearMonthLoader currentYmLoader;
        YearMonth currentYm;
        EventOperator & eo;
        std::unique_ptr<PeriodManager> periodManager;
        Account & acc;
        std::vector<PeriodWidget *> periodWidgets;

    public:
        explicit MainWindow(size_t maxPeriodsToShow,
                            std::string currentYearMonthPath = "currentyearmonth.txt",
                            std::string eventsPath = "events.txt",
                            std::string accountPath = "account.txt",
                            QWidget * parent = 0);
        MainWindow(const MainWindow &) = delete;
        void operator=(const MainWindow &) = delete;
//        void setEventsPath(const std::string & value);
//        void setAccountPath(const std::string & value);

    private slots:
        void save();

    public slots:
        void addedEvent(EventShptr es);

    };
}}

#endif // MAINWINDOW_H
