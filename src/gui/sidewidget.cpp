/*
 * Copyright (C) 2018 Peter Szabo
 *
 * This file is part of MoneyManager.
 *
 * MoneyManager is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MoneyManager is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MoneyManager.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <QPushButton>
#include <QVBoxLayout>
#include <QString>

#include "sidewidget.h"
#include "eventwidget.h"
#include "eventdetailsdialog.h"
#include "../common.h"
#include "../eventoperator.h"

using namespace mm::gui;

SideWidget::SideWidget(YearMonth currentYm, Account & acc, QWidget * parent)
    : QWidget(parent), currentYm(currentYm), acc(acc), total(0)
{
    setMinimumWidth(140);

    QVBoxLayout * layout = new QVBoxLayout(this);
    setLayout(layout);

    QPushButton * addEventBtn = new QPushButton("New event", this);
    connect(addEventBtn, SIGNAL(clicked()), this, SLOT(onNewEvent()));
    layout->addWidget(addEventBtn);

    // closing a period will send the remaining events to the next period
    // and it will be manual by design
    QPushButton * periodClosureBtn = new QPushButton("Period closure", this);
    layout->addWidget(periodClosureBtn);

    layout->addStretch();

    QLabel * cashLabel = new QLabel("Cash", this);
    layout->addWidget(cashLabel);
    cash = new MoneySpinBox(this);
    cash->setValue(acc.getCash());
    connect(cash, SIGNAL(valueChangedBy(Money)), this, SLOT(onCashChangeBy(Money)));
    layout->addWidget(cash);

    layout->addStretch();

    QLabel * bankLabel = new QLabel("Bank account", this);
    layout->addWidget(bankLabel);
    bank = new MoneySpinBox(this);
    bank->setValue(acc.getBank());
    connect(bank, SIGNAL(valueChangedBy(Money)), this, SLOT(onBankChangeBy(Money)));
    layout->addWidget(bank);

    layout->addStretch();

    QLabel * savingLabel = new QLabel("Saving", this);
    layout->addWidget(savingLabel);
    saving = new MoneySpinBox(this);
    saving->setValue(acc.getSaving());
    connect(saving, SIGNAL(valueChangedBy(Money)), this, SLOT(onSavingChangeBy(Money)));
    layout->addWidget(saving);

    layout->addStretch();

    QLabel * totalLabel = new QLabel("Total", this);
    layout->addWidget(totalLabel);
    totalAmount = new QLabel(this);
    layout->addWidget(totalAmount);

    total = acc.getCash() + acc.getBank() + acc.getSaving();
    totalAmount->setText(QString::number(total));

    layout->addStretch();
}

void SideWidget::onNewEvent()
{
    EventDetailsDialog edd{currentYm, this};
    EventShptr es = edd.run();
    emit addedEvent(es);
}

void SideWidget::onCashChangeBy(mm::Money amount)
{
    acc.setCash(cash->value());
    onMoneyChangeBy(amount);
}

void SideWidget::onBankChangeBy(mm::Money amount)
{
    acc.setBank(bank->value());
    onMoneyChangeBy(amount);
}

void SideWidget::onSavingChangeBy(mm::Money amount)
{
    acc.setSaving(saving->value());
    onMoneyChangeBy(amount);
}

void SideWidget::onMoneyChangeBy(mm::Money amount)
{
    total += amount;
    totalAmount->setText(QString::number(total));
    acc.saveToFile();
}
