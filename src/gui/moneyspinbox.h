/*
 * Copyright (C) 2018 Peter Szabo
 *
 * This file is part of MoneyManager.
 *
 * MoneyManager is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MoneyManager is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MoneyManager.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef MONEYSPINBOX_H
#define MONEYSPINBOX_H

#include <QSpinBox>

#include "../common.h"

namespace mm { namespace gui
{
    class MoneySpinBox : public QSpinBox
    {
        Q_OBJECT
        int oldAmount;

    public:
        explicit MoneySpinBox(QWidget * parent = 0);

        // won't emit the valueChangedBy(Money) signal
        // which is useful when the "underlying" event's price has changed from other sources
        // and the view needs to be refreshed
        void setValueWithoutEmit(int val);

    signals:
        void valueChangedBy(Money amount);

    private slots:
        void onValueChange(int amount);

    };
}}

#endif // MONEYSPINBOX_H
