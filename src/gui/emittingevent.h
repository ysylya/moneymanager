/*
 * Copyright (C) 2018 Peter Szabo
 *
 * This file is part of MoneyManager.
 *
 * MoneyManager is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MoneyManager is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MoneyManager.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef EMITTINGEVENT_H
#define EMITTINGEVENT_H

#include <QObject>

#include "../event.h"

namespace mm { namespace gui
{
    // this class is useful for emitting signals to all the widgets which contains it
    class EmittingEvent : public QObject
    {
        Q_OBJECT
        std::unique_ptr<Event> event;
        bool enabled; // false means it's done or cancelled (so won't be saved)

    public:
        // can't have a parent because they will be hold in a shared_ptr
        // it'd be dangerous to mix QT parent system with shared_ptr!
        explicit EmittingEvent(std::unique_ptr<Event> event);

        bool isActiveThatYearMonth(YearMonth yearMonth);
        std::string save();
        bool load(std::string data);
        void finished();

        std::string getDescription() const;
        void setDescription(const std::string & value);
        Money getPrice() const;
        void setPrice(const Money & value); // TODO delete it because it won't emit: could be confusing
        void changePriceBy(const Money & value);
        AccountType getAccountType() const;
        void setAccountType(const AccountType & value);
        void disable();
        bool isEnabled();

    signals:
        void saveSignal();
        void eventIsDisabled();
        void descriptionIsUpdated();
        void priceIsChangedBy(Money);

    public slots:

    };
}}

#endif // EMITTINGEVENT_H
