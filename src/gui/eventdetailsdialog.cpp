/*
 * Copyright (C) 2018 Peter Szabo
 *
 * This file is part of MoneyManager.
 *
 * MoneyManager is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MoneyManager is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MoneyManager.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <QComboBox>
#include <QDialogButtonBox>
#include <QFormLayout>
#include <QLineEdit>
#include <QPushButton>
#include <QHBoxLayout>
#include <QCheckBox>
#include <QLabel>

#include "../event.h"
#include "../period.h"
#include "../fixedevent.h"
#include "../periodicevent.h"
#include "eventdetailsdialog.h"
#include "moneyspinbox.h"
#include "emittingevent.h"

using namespace mm::gui;

EventDetailsDialog::EventDetailsDialog(YearMonth currentYm, QWidget * parent)
    : QDialog(parent)
{
    QVBoxLayout * mainLayout = new QVBoxLayout(this);
    setLayout(mainLayout);

    setupFormLayout(currentYm);
    mainLayout->addLayout(formLayout);

    QHBoxLayout * buttonLayout = new QHBoxLayout();
    mainLayout->addLayout(buttonLayout);

    QDialogButtonBox * buttonBox = new QDialogButtonBox();
    buttonBox->addButton("Ok", QDialogButtonBox::AcceptRole);
    buttonBox->addButton("Cancel", QDialogButtonBox::RejectRole);
    connect(buttonBox, SIGNAL(accepted()), this, SLOT(accept()));
    connect(buttonBox, SIGNAL(rejected()), this, SLOT(reject()));
    mainLayout->addWidget(buttonBox);
}

mm::EventShptr EventDetailsDialog::run()
{
    if (exec() == QDialog::Accepted)
    {
        EventShptr es;
        std::unique_ptr<Event> event;
        switch (type->currentIndex())
        {
            case 0:
                event = std::unique_ptr<FixedEvent>(new FixedEvent(description->text().toUtf8().constData(),
                                                                   price->value(),
                                                                   fixedPsWidget->getActiveYearMonth(),
                                                                   getAccountType(),
                                                                   fixedPsWidget->getDuration()));
                es = std::make_shared<EmittingEvent>(std::move(event));
                break;
            case 1:
                event = std::unique_ptr<PeriodicEvent>(new PeriodicEvent(description->text().toUtf8().constData(),
                                                                         price->value(),
                                                                         periodicPsWidget->getActivePeriods(),
                                                                         getAccountType()));
                es = std::make_shared<EmittingEvent>(std::move(event));
                break;
        }
        return es;
    }
    return nullptr;
}

void EventDetailsDialog::setupFormLayout(YearMonth currentYm)
{
    formLayout = new QFormLayout(); // there is a warning with "this" arg

    description = new QLineEdit(this);
    formLayout->addRow("Description", description);

    price = new MoneySpinBox(this);
    formLayout->addRow("Price", price);

    accType = new QComboBox(this);
    accType->addItem("by cash");
    accType->addItem("from a bank account");
    accType->addItem("from the savings");
    formLayout->addRow("It'll be paid", accType);

    type = new QComboBox(this);
//    type->addItem("Fixed");
//    type->addItem("Periodic");
//    formLayout->addRow("Type", type);

    type->addItem("in a fixed time (interval)");
    type->addItem("periodically");
    formLayout->addRow("It happens", type);

    fixedPsWidget = new FixedPsWidget(currentYm, this);
    periodicPsWidget = new PeriodicPsWidget(6, 2, this); // 6 * 2 = 12 months
    periodicPsWidget->hide();

    periodSelectorWidget = new PeriodSelectorWidget(this);
    periodSelectorWidget->setActualPsWidget(fixedPsWidget);

    formLayout->addRow("Select Period", periodSelectorWidget);

    connect(type, SIGNAL(currentIndexChanged(int)), this, SLOT(setupPeriodSelector(int)));
}

mm::AccountType EventDetailsDialog::getAccountType()
{
    switch (accType->currentIndex())
    {
        case 0: return AccountType::Cash;
        case 1: return AccountType::Bank;
        case 2: return AccountType::Savings;
        default: throw "mm::AccountType EventDetailsDialog::getAccountType()";
    }
}

void EventDetailsDialog::setupPeriodSelector(int index)
{
    switch (type->currentIndex())
    {
        case 0:
            periodicPsWidget->hide();
            fixedPsWidget->show();
            periodSelectorWidget->setActualPsWidget(fixedPsWidget);
            break;
        case 1:
            fixedPsWidget->hide();
            periodicPsWidget->show();
            periodSelectorWidget->setActualPsWidget(periodicPsWidget);
            break;
    }
}


EventDetailsDialog::PeriodSelectorWidget::PeriodSelectorWidget(QWidget * parent)
    : QWidget(parent), psw(nullptr)
{
    setFixedWidth(200);
    layout = new QVBoxLayout(this);
    layout->setMargin(0);
    layout->setSpacing(0);
    setLayout(layout);
}

bool EventDetailsDialog::PeriodSelectorWidget::isActiveAt(mm::YearMonth ym)
{
    // TODO if (psw == nullptr)
    return psw->isActiveAt(ym);
}

void EventDetailsDialog::PeriodSelectorWidget::setActualPsWidget(EventDetailsDialog::PeriodSelectorWidget * psw)
{
    layout->removeWidget(this->psw);
    this->psw = psw;
    layout->addWidget(psw);
}


EventDetailsDialog::FixedPsWidget::FixedPsWidget(YearMonth currentYm, QWidget * parent)
    : PeriodSelectorWidget(parent)
{
    QLabel * from = new QLabel("from:");
    from->setAlignment(Qt::AlignCenter);
    layout->addWidget(from);

    startYear = new QComboBox(this);
    for (int i = 0; i < 3; ++i)
    {
        startYear->addItem(QString::number(currentYm.year + i));
    }
    layout->addWidget(startYear);

    startMonth = new QComboBox(this);
    for (int i = 1; i <= NUM_MONTHS; ++i)
    {
        startMonth->addItem(QString::fromStdString(MONTHS[i]));
    }
    layout->addWidget(startMonth);

    QLabel * to = new QLabel("to:");
    to->setAlignment(Qt::AlignCenter);
    layout->addWidget(to);

    endYear = new QComboBox(this);
    for (int i = 0; i < 3; ++i)
    {
        endYear->addItem(QString::number(currentYm.year + i));
    }
    layout->addWidget(endYear);

    endMonth = new QComboBox(this);
    for (int i = 1; i <= NUM_MONTHS; ++i)
    {
        endMonth->addItem(QString::fromStdString(MONTHS[i]));
    }
    layout->addWidget(endMonth);
}

bool EventDetailsDialog::FixedPsWidget::isActiveAt(mm::YearMonth ym)
{
    YearMonth active{startYear->currentText().toInt(), startMonth->currentIndex() + 1};
    return ym == active;
}

mm::YearMonth EventDetailsDialog::FixedPsWidget::getActiveYearMonth()
{
    YearMonth fromYM {startYear->currentText().toInt(), startMonth->currentIndex() + 1};
    YearMonth toYM {endYear->currentText().toInt(), endMonth->currentIndex() + 1};

    return fromYM <= toYM ? fromYM : toYM; // returns the lower boundary
}

int EventDetailsDialog::FixedPsWidget::getDuration()
{
    YearMonth fromYM {startYear->currentText().toInt(), startMonth->currentIndex() + 1};
    YearMonth toYM {endYear->currentText().toInt(), endMonth->currentIndex() + 1};
    int duration = toYM - fromYM;

    // since getActiveYearMonth() returns the lower boundary,
    // this needs to return the duration in absolute values
    return std::abs(duration);
}


EventDetailsDialog::PeriodicPsWidget::PeriodicPsWidget(int rows, int columns, QWidget * parent)
    : PeriodSelectorWidget(parent), rows(rows), columns(columns)
{
    if (rows * columns != NUM_MONTHS)
    {
        throw std::invalid_argument("rows * columns != NUM_MONTHS");
    }

    for (int i = 0; i < NUM_MONTHS; ++i)
    {
        checkBoxes[i] = new QCheckBox(QString::fromStdString(MONTHS[i+1]), this);
    }

    gridLayout = new QGridLayout();
    for (int row = 0; row < rows; ++row)
    {
        for (int col = 0; col < columns; ++col)
        {
            int month = row + col * rows;
            gridLayout->addWidget(checkBoxes.at(month), row, col);
        }
    }
    layout->addLayout(gridLayout);
}

bool EventDetailsDialog::PeriodicPsWidget::isActiveAt(mm::YearMonth ym)
{
    return checkBoxes[ym.month]->isChecked();
}

std::array<bool, mm::NUM_MONTHS> EventDetailsDialog::PeriodicPsWidget::getActivePeriods()
{
    std::array<bool, NUM_MONTHS> active;
    for (int i = 0; i < NUM_MONTHS; ++i)
    {
        active[i] = (checkBoxes[i]->isChecked());
    }
    return active;
}
