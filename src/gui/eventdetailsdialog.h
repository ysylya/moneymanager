/*
 * Copyright (C) 2018 Peter Szabo
 *
 * This file is part of MoneyManager.
 *
 * MoneyManager is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MoneyManager is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MoneyManager.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef EVENTDETAILSDIALOG_H
#define EVENTDETAILSDIALOG_H

#include <QAbstractButton>
#include <QCheckBox>
#include <QComboBox>
#include <QDialog>
#include <QFormLayout>
#include <array>

#include "../common.h"
#include "../yearmonth.h"
#include "../account.h"
#include "moneyspinbox.h"

namespace mm { namespace gui
{
    class EventDetailsDialog : public QDialog
    {
        Q_OBJECT
        QFormLayout * formLayout;
        QLineEdit * description;
        MoneySpinBox * price;
        QComboBox * type;
        QComboBox * accType;

        class PeriodSelectorWidget;
        class FixedPsWidget;
        class PeriodicPsWidget;
        PeriodSelectorWidget * periodSelectorWidget;
        FixedPsWidget * fixedPsWidget;
        PeriodicPsWidget * periodicPsWidget;

    public:
        explicit EventDetailsDialog(YearMonth currentYm, QWidget * parent = 0);
        EventShptr run();

    private:
        void setupFormLayout(YearMonth currentYm);
        AccountType getAccountType();

    private slots:
        void setupPeriodSelector(int index);

    private:
        class PeriodSelectorWidget : public QWidget
        {
        protected:
            QVBoxLayout * layout;
            PeriodSelectorWidget * psw;

        public:
            PeriodSelectorWidget(QWidget * parent = 0);
            virtual bool isActiveAt(YearMonth ym);

            // This is a helper function for switching between Fixed- and PeriodicPsWidget
            // in the formLayout. This version of QT doesn't support deleting a row from the FormLayout class
            // so this will help simulate that behaviour
            void setActualPsWidget(PeriodSelectorWidget * psw);
        };


        class FixedPsWidget : public PeriodSelectorWidget
        {
            QComboBox * startYear, * startMonth, * endYear, * endMonth;

        public:
            FixedPsWidget(YearMonth currentYm, QWidget * parent = 0);
            virtual bool isActiveAt(YearMonth ym) override;
            YearMonth getActiveYearMonth();
            int getDuration();

        };


        class PeriodicPsWidget : public PeriodSelectorWidget
        {
            QGridLayout * gridLayout;
            std::array<QCheckBox *, NUM_MONTHS> checkBoxes;
            int rows;
            int columns;

        public:
            PeriodicPsWidget(int rows, int columns, QWidget * parent = 0);
            virtual bool isActiveAt(YearMonth ym) override;
            std::array<bool, NUM_MONTHS> getActivePeriods();

        };

    };

}}

#endif // EVENTDETAILSDIALOG_H
