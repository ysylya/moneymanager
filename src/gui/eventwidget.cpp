/*
 * Copyright (C) 2018 Peter Szabo
 *
 * This file is part of MoneyManager.
 *
 * MoneyManager is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MoneyManager is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MoneyManager.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <QHBoxLayout>
#include <QPushButton>

#include "eventwidget.h"
#include "../common.h"
#include "../event.h"
#include "../fixedevent.h"
#include "../eventoperator.h"

using namespace mm::gui;

// Layout and widget dimensions
const int LAYOUT_SPACING = 5;
const int DESCRIPTION_WIDTH = 160;
const int PRICE_WIDTH = 80;
const int DETAILS_WIDTH = 20;
const int DONE_WIDTH = 50;
const int CANCEL_WIDTH = 70;

int EventWidget::getWidth()
{
    return 4*LAYOUT_SPACING + DESCRIPTION_WIDTH + PRICE_WIDTH
            + DETAILS_WIDTH + DONE_WIDTH + CANCEL_WIDTH;
}

EventWidget::EventWidget(EventShptr event, QWidget * parent)
    : QWidget(parent), event(event)
{
    id = nextId;
    ++nextId;

    setFixedWidth(getWidth());

    QHBoxLayout * layout = new QHBoxLayout(this);
    layout->setMargin(0);
    layout->setSpacing(LAYOUT_SPACING);
    setLayout(layout);

    description = new MoneyLineEdit(this);
    description->setText(QString::fromUtf8(event->getDescription().c_str()));
    description->setFixedWidth(DESCRIPTION_WIDTH);
    connect(description, SIGNAL(textChanged(QString)), this, SLOT(onDescriptionChanged(QString)));
    layout->addWidget(description);

    price = new MoneySpinBox(this);
    price->setValue(event->getPrice());
    price->setFixedWidth(PRICE_WIDTH);
    connect(price, SIGNAL(valueChangedBy(Money)), this, SLOT(onPriceChanged(Money)));
    layout->addWidget(price);

    QPushButton * details = new QPushButton(this);
    details->setText("...");
    details->setFixedWidth(DETAILS_WIDTH);
    // TODO will open a new window
    layout->addWidget(details);

    QPushButton * done = new QPushButton(this);
    done->setText("Done"); // TODO translation
    done->setFixedWidth(DONE_WIDTH);
    connect(done, SIGNAL(clicked()), this, SIGNAL(done()));
    layout->addWidget(done);

    QPushButton * cancel = new QPushButton(this);
    cancel->setText("Cancel"); // TODO translation
    cancel->setFixedWidth(DONE_WIDTH);
    connect(cancel, SIGNAL(clicked()), this, SIGNAL(cancel()));
    layout->addWidget(cancel);

    // whenever an event changes, it sets all the associated EWs
    connect(event.get(), SIGNAL(saveSignal()), this, SIGNAL(save()));
    connect(event.get(), SIGNAL(descriptionIsUpdated()), this, SLOT(refreshGui()));
    connect(event.get(), SIGNAL(priceIsChangedBy(Money)), this, SLOT(refreshGui()));
}

void EventWidget::onDescriptionChanged(QString newDescription)
{
    event->setDescription(newDescription.toUtf8().constData());
}

void EventWidget::onPriceChanged(Money amount)
{
    event->changePriceBy(amount);
}

void EventWidget::refreshGui()
{
    description->setText(QString::fromUtf8(event->getDescription().c_str()));
    price->setValueWithoutEmit(event->getPrice());
}

int EventWidget::nextId = 0;

int EventWidget::getId() const
{
    return id;
}

mm::EventShptr EventWidget::getEvent()
{
    return event;
}


EventContainerWidget::EventContainerWidget(EventShptrVec & events, QWidget * parent)
    : QWidget(parent), events(events)
{
    setFixedWidth(EventWidget::getWidth());

    layout = new QVBoxLayout(this);
    layout->setMargin(0);
    layout->setSpacing(10);
    layout->setAlignment(Qt::AlignTop);
    setLayout(layout);

    for (EventShptr & event : events)
    {
        setupEvent(event);
    }
}

void EventContainerWidget::setupEvent(EventShptr & eventShptr)
{
    // onDone() and onCancel() are invoking the disabling method on the EmittingEvent
    // which signals the eventIsDisabled() to _every_ ECW
    // so it's cleared from everywhere
    connect(eventShptr.get(), SIGNAL(eventIsDisabled()), this, SLOT(removeDisabledEventWidgets()));
    connect(eventShptr.get(), SIGNAL(priceIsChangedBy(Money)), this, SIGNAL(somePriceIsChangedBy(Money)));
    connect(eventShptr.get(), SIGNAL(saveSignal()), this, SIGNAL(save()));

    EventWidget * ew = new EventWidget(eventShptr, this);

    connect(ew, SIGNAL(done()), this, SLOT(onDone()));
    connect(ew, SIGNAL(cancel()), this, SLOT(onCancel()));

    layout->addWidget(ew);
}

void EventContainerWidget::addEvent(EventShptr & eventShptr)
{
    events.push_back(eventShptr);

    setupEvent(eventShptr);
}

void EventContainerWidget::disableEventWidget(int id, bool done)
{
    for (int i = 0; i < layout->count(); ++i)
    {
        EventWidget * ew = qobject_cast<EventWidget *>(layout->itemAt(i)->widget());
        if (ew != 0 && ew->getId() == id) // qobject_cast returns 0 for non-EventWidget*
        {
            for (EventShptr & es : events)
            {
                if (es == ew->getEvent())
                {
                    if (done)
                    {
                        // later in the signal chain every periodwidget's ecw's removeDisabledEventWidgets()
                        // will be called, which emits the somePriceHasChangedBy(-x) func,
                        // (this is to refresh every periodwidget on e. g. a periodicevent cancellation
                        // but if it's done it's removing money from the budget, so doesn't need refresh
                        // therefore it is set to 0

                        // TODO done is removing money from the accounts
                        es->setPrice(0);
                    }
                    es->disable();
                    emit save();
                }
            }
            break;
        }
    }
}

void EventContainerWidget::onDone()
{
    int id = qobject_cast<EventWidget *>(sender())->getId();
    disableEventWidget(id, true);
}

void EventContainerWidget::onCancel()
{
    int id = qobject_cast<EventWidget *>(sender())->getId();
    disableEventWidget(id, false);
}

void EventContainerWidget::removeDisabledEventWidgets()
{
    for (int i = 0; i < layout->count(); ++i)
    {
        EventWidget * ew = qobject_cast<EventWidget *>(layout->itemAt(i)->widget());
        if (ew != 0 && ew->getEvent()->isEnabled() == false)
        {
            emit somePriceIsChangedBy(-ew->getEvent()->getPrice());
            delete ew;
        }
    }
}
