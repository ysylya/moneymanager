/*
 * Copyright (C) 2018 Peter Szabo
 *
 * This file is part of MoneyManager.
 *
 * MoneyManager is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MoneyManager is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MoneyManager.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "emittingevent.h"

using namespace mm::gui;

EmittingEvent::EmittingEvent(std::unique_ptr<Event> event)
    : QObject(0), event(std::move(event)), enabled(true)
{
}

bool EmittingEvent::isActiveThatYearMonth(mm::YearMonth yearMonth)
{
    return event->isActiveThatYearMonth(yearMonth);
}

std::string EmittingEvent::save()
{
    return event->save();
}

bool EmittingEvent::load(std::string data)
{
    return event->load(data);
}

void EmittingEvent::finished()
{
    event->finished();
}

std::string EmittingEvent::getDescription() const
{
    return event->getDescription();
}

void EmittingEvent::setDescription(const std::string & value)
{
    event->setDescription(value);
    emit descriptionIsUpdated();
    emit saveSignal();
}

mm::Money EmittingEvent::getPrice() const
{
    return event->getPrice();
}

void EmittingEvent::setPrice(const mm::Money & value)
{
    event->setPrice(value);
}

void EmittingEvent::changePriceBy(const mm::Money & value)
{
    event->changePriceBy(value);
    emit priceIsChangedBy(value);
    emit saveSignal();
}

mm::AccountType EmittingEvent::getAccountType() const
{
    return event->getAccountType();
}

void EmittingEvent::setAccountType(const mm::AccountType & value)
{
    event->setAccountType(value);
}

void EmittingEvent::disable()
{
    enabled = false;
    emit eventIsDisabled();
}

bool EmittingEvent::isEnabled()
{
    return enabled;
}
