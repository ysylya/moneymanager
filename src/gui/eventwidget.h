/*
 * Copyright (C) 2018 Peter Szabo
 *
 * This file is part of MoneyManager.
 *
 * MoneyManager is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MoneyManager is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MoneyManager.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef EVENTWIDGET_H
#define EVENTWIDGET_H

#include <QWidget>
#include <QString>
#include <QVBoxLayout>

#include "../common.h"
#include "moneylineedit.h"
#include "moneyspinbox.h"

namespace mm { namespace gui
{
    class EventWidget : public QWidget
    {
        Q_OBJECT
        MoneyLineEdit * description;
        MoneySpinBox * price;
        static int nextId;
        int id;
        EventShptr event;

    public:
        explicit EventWidget(EventShptr event = 0, QWidget * parent = 0);
        int getId() const;
        EventShptr getEvent();
        static int getWidth();

    signals:
        void updatedDescription();
        void updatedPriceBy(Money);
        void updatedDetails();
        void done();
        void cancel();
        void save();

    private slots:
        void onDescriptionChanged(QString newDescription);
        void onPriceChanged(Money amount);
        void refreshGui();

    };

    class EventContainerWidget : public QWidget
    {
        Q_OBJECT
        QVBoxLayout * layout;
        EventShptrVec events;

    public:
        explicit EventContainerWidget(EventShptrVec & events, QWidget * parent = 0);

    private:
        void setupEvent(EventShptr & event);

    signals:
        void save();
        void somePriceIsChangedBy(Money);

    public slots:
        void addEvent(EventShptr & eventShptr);
        void disableEventWidget(int id, bool done);

    private slots:
        void onDone();
        void onCancel();
        void removeDisabledEventWidgets();

    };
}}

#endif // EVENTWIDGET_H
