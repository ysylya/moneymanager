/*
 * Copyright (C) 2018 Peter Szabo
 *
 * This file is part of MoneyManager.
 *
 * MoneyManager is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MoneyManager is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MoneyManager.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef PERIODWIDGET_H
#define PERIODWIDGET_H

#include <QWidget>
#include <QLabel>
#include <QPushButton>
#include <QScrollArea>

#include "../common.h"
#include "../period.h"

#include "eventwidget.h"

namespace mm { namespace gui
{
    class PeriodWidget : public QWidget
    {
        Q_OBJECT
        Period & period;
        QLabel * name;
        QScrollArea * ecwScroll;
        EventContainerWidget * ecw;
        QPushButton * addEventBtn;
        QLabel * totalLabel;
        int total;

    public:
        explicit PeriodWidget(Period & period, QWidget * parent = 0);
        static int getWidth();
        static int getHeight();
        Period & getPeriod();
        void setPeriod(Period & p);
        void addEvent(EventShptr & eventShptr);

    signals:
        void save();
        void newEvent(EventShptr);

    public slots:
        void onNewEvent();
        void changeTotalBy(Money amount);

    };
}}

#endif // PERIODWIDGET_H
