/*
 * Copyright (C) 2018 Peter Szabo
 *
 * This file is part of MoneyManager.
 *
 * MoneyManager is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MoneyManager is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MoneyManager.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef SIDEWIDGET_H
#define SIDEWIDGET_H

#include <QWidget>
#include <QLabel>

#include "../common.h"
#include "../account.h"
#include "../periodmanager.h"
#include "moneyspinbox.h"

namespace mm { namespace gui
{
    class SideWidget : public QWidget
    {
        Q_OBJECT

        YearMonth currentYm;
        Account & acc;

        MoneySpinBox * cash;
        MoneySpinBox * bank;
        MoneySpinBox * saving;
        QLabel * totalAmount;
        Money total;

    public:
        SideWidget(YearMonth currentYm, Account & acc, QWidget * parent = 0);

    signals:
        void totalHasChangedBy(Money amount);
        void addedEvent(EventShptr es);

    private slots:
        void onNewEvent();
        void onCashChangeBy(Money amount);
        void onBankChangeBy(Money amount);
        void onSavingChangeBy(Money amount);
        void onMoneyChangeBy(Money amount);

    };
}}

#endif // SIDEWIDGET_H
