/*
 * Copyright (C) 2018 Peter Szabo
 *
 * This file is part of MoneyManager.
 *
 * MoneyManager is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MoneyManager is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MoneyManager.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <QHBoxLayout>

#include "mainwindow.h"
#include "sidewidget.h"

using namespace mm::gui;

MainWindow::MainWindow(size_t maxPeriodsToShow, std::string currentYearMonthPath,
                       std::string eventsPath, std::string accountPath, QWidget * parent)
    : QWidget(parent), eo(EventOperator::getInstance()), acc(Account::getInstance())
{
    currentYmLoader.setPath(currentYearMonthPath);
    currentYmLoader.loadFromFile();
    currentYm = currentYmLoader.getCurrent();

    eo.setPath(eventsPath);
    eo.loadFromFile(); // TODO exception if fails

    acc.setPath(accountPath);
    acc.loadFromFile();

    // choosing which event has connection with which period
    periodManager = std::unique_ptr<PeriodManager>(new PeriodManager{currentYm, maxPeriodsToShow, eo.getEvents()});

    // layout
    int innerMargin = 5;
    int outerMargin = 5;

    QWidget * innerWidget = new QWidget(this);
    QHBoxLayout * innerLayout = new QHBoxLayout(innerWidget);
    innerLayout->setAlignment(Qt::AlignTop);
    innerWidget->setLayout(innerLayout);
    innerLayout->setMargin(innerMargin);

    QHBoxLayout * outerLayout = new QHBoxLayout(this);
    outerLayout->setMargin(outerMargin);

    SideWidget * sw = new SideWidget(currentYm, acc, this);
    connect(sw, SIGNAL(addedEvent(EventShptr)), this, SLOT(addedEvent(EventShptr)));
    outerLayout->addWidget(sw);

    for (size_t i = 0; i < maxPeriodsToShow; ++i)
    {
        PeriodWidget * pw = new PeriodWidget(periodManager->at(i), this);
        periodWidgets.push_back(pw);
        connect(pw, SIGNAL(newEvent(EventShptr)), this, SLOT(addedEvent(EventShptr)));
        connect(pw, SIGNAL(save()), this, SLOT(save()));
        innerLayout->addWidget(pw);
        innerLayout->addStretch();
    }

    QScrollArea * scroll = new QScrollArea(this);
    scroll->setWidget(innerWidget);
    scroll->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOn);

    outerLayout->addWidget(scroll);
    setLayout(outerLayout);

    int pwWidth  = PeriodWidget::getWidth() * 3; // the first 3 Period has to be seen and a bit from the 4.
    int pwHeight = PeriodWidget::getHeight(); // TODO for now +40 for padding, etc

    setMinimumWidth(pwWidth + sw->width() + innerMargin * 4 + outerMargin * 3);
    setMinimumHeight(pwHeight + innerMargin * 3 + 40); // +40 for the scrollbar
}

void MainWindow::save()
{
    eo.saveToFile();
}

void MainWindow::addedEvent(EventShptr es)
{
    if (es == nullptr)
    {
        return;
    }

    for (PeriodWidget * pw : periodWidgets)
    {
        YearMonth periodYM = pw->getPeriod().getYearMonth();
        if (es->isActiveThatYearMonth(periodYM))
        {
            pw->addEvent(es);
        }
    }

    periodManager->addNewEvent(es);
    save();
}
