/*
 * Copyright (C) 2018 Peter Szabo
 *
 * This file is part of MoneyManager.
 *
 * MoneyManager is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MoneyManager is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MoneyManager.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "moneyspinbox.h"

using namespace mm::gui;

MoneySpinBox::MoneySpinBox(QWidget * parent)
    : QSpinBox(parent), oldAmount(0)
{
    setRange(-10000000, 10000000);
    setSingleStep(1000);
    setKeyboardTracking(false);
    connect(this, SIGNAL(valueChanged(int)), this, SLOT(onValueChange(int)));
}

void MoneySpinBox::setValueWithoutEmit(int val)
{
    blockSignals(true);
    QSpinBox::setValue(val);
    blockSignals(false);
//    oldAmount = val;
}

void MoneySpinBox::onValueChange(int amount)
{
    emit valueChangedBy(amount - oldAmount);
    oldAmount = amount;
}
