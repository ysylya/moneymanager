/*
 * Copyright (C) 2018 Peter Szabo
 *
 * This file is part of MoneyManager.
 *
 * MoneyManager is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MoneyManager is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MoneyManager.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <QVBoxLayout>
#include <QString>

#include "periodwidget.h"
#include "../eventoperator.h"
#include "../fixedevent.h"

using namespace mm::gui;

PeriodWidget::PeriodWidget(Period & period, QWidget * parent)
    : QWidget(parent), period(period), total(0)
{
    setFixedWidth(getWidth());
    setFixedHeight(getHeight());

    QVBoxLayout * layout = new QVBoxLayout(this);
    layout->setMargin(0);
    layout->setSpacing(10);
    layout->setAlignment(Qt::AlignTop);
    setLayout(layout);

    name = new QLabel("Period name", this);
    name->setAlignment(Qt::AlignCenter);
    layout->addWidget(name);

    ecwScroll = new QScrollArea(this);
    ecw = new EventContainerWidget(period.getEvents(), ecwScroll);
    ecwScroll->setWidget(ecw);
    ecwScroll->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    ecwScroll->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
    ecwScroll->setWidgetResizable(true);
    // TODO autoscroll down when a new item is added
    layout->addWidget(ecwScroll);

    addEventBtn = new QPushButton("New event", this);
    // the connect(addEventBtn, SIGNAL(clicked()), this, SLOT(addNewEvent()));
    // is implemented in setPeriod, not here, because a later set period needs
    // to override, and if it is set here, it would be set twice
    layout->addWidget(addEventBtn);

    totalLabel = new QLabel(this);
    changeTotalBy(0);
    layout->addWidget(totalLabel);

    setPeriod(period);
}

int PeriodWidget::getWidth()
{
    return EventWidget::getWidth() + 10; // +10 for the scrollbar
}

int PeriodWidget::getHeight()
{
    return 680; // It looks nice on 720p and higher
}

mm::Period & PeriodWidget::getPeriod()
{
    return period;
}

void PeriodWidget::setPeriod(Period & p)
{
    period = p;

    name->setText(QString::fromUtf8(period.getName().c_str()));

    delete ecw; // not sure if it's needed
    ecw = new EventContainerWidget(period.getEvents(), ecwScroll);
    ecwScroll->setWidget(ecw);
    connect(ecw, SIGNAL(somePriceIsChangedBy(Money)), this, SLOT(changeTotalBy(Money)));
    connect(ecw, SIGNAL(save()), this, SIGNAL(save()));

    // the signal-slot connection happens later than the ctor call,
    // so I need to calculate the initial amount manually
    // because changeTotalBy(int) won't catch the updatedPriceBy(int) signal from the ctor
    int initialAmount = 0;
    for (EventShptr & event: period.getEvents())
    {
        initialAmount += event->getPrice();
    }
    total = 0;
    changeTotalBy(initialAmount);

    connect(addEventBtn, SIGNAL(clicked()), this, SLOT(onNewEvent()));
}

void PeriodWidget::addEvent(EventShptr & eventShptr)
{
    ecw->addEvent(eventShptr);
    changeTotalBy(eventShptr->getPrice());
}

void PeriodWidget::onNewEvent()
{
    std::unique_ptr<FixedEvent> fe{new FixedEvent("new", 0, period.getYearMonth())};
    EventShptr es = std::make_shared<EmittingEvent>(std::move(fe));
    emit newEvent(es);
}

void PeriodWidget::changeTotalBy(Money amount)
{
    total += amount;
    QString summary {"Summary: " + QString::number(total)};
    totalLabel->setText(summary);
}
