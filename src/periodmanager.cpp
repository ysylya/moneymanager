/*
 * Copyright (C) 2018 Peter Szabo
 *
 * This file is part of MoneyManager.
 *
 * MoneyManager is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MoneyManager is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MoneyManager.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <iostream>

#include "periodmanager.h"

using namespace mm;

PeriodManager::PeriodManager(YearMonth currentYM, size_t maxPeriodsToShow, EventShptrVec & events)
    : currentYM(currentYM), maxPeriodsToShow(maxPeriodsToShow), events(events)
{
    for (size_t i = 0; i < maxPeriodsToShow; ++i)
    {
        periods.push_back(Period{YearMonth{currentYM + i}});
    }

    for (EventShptr & event : events)
    {
        addExistingEvent(event);
    }
}

PeriodManager::PeriodManager(const PeriodManager & other)
    : currentYM(other.currentYM), maxPeriodsToShow(other.maxPeriodsToShow),
      events(other.events), periods(other.periods)
{

}

/**
 * @brief Month-distance from currentYM
 */
Period & PeriodManager::operator[](size_t i)
{
    return periods[i];
}

/**
 * @brief Month-distance from currentYM
 */
Period & PeriodManager::at(size_t i)
{
    return periods.at(i);
}

size_t PeriodManager::getMaxPeriodsToShow() const
{
    return maxPeriodsToShow;
}

void PeriodManager::setMaxPeriodsToShow(int value)
{
    maxPeriodsToShow = value;
}

void PeriodManager::addNewEvent(EventShptr & event)
{
    if (event == nullptr)
    {
        return;
    }

    events.push_back(event);

    addExistingEvent(event);
}

void PeriodManager::addExistingEvent(EventShptr & event)
{
    if (event == nullptr)
    {
        return;
    }

    for (size_t i = 0; i < maxPeriodsToShow; ++i)
    {
        if (event->isActiveThatYearMonth(currentYM + i))
        {
            periods.at(i).addEvent(event);
        }
    }
}

void PeriodManager::printEvents()
{
    for (Period & p : periods)
    {
        std::cout << p.getYearMonth().year << " " << p.getYearMonth().month << std::endl;
        for (size_t i = 0; i < p.numEvents(); ++i)
        {
            mm::EventShptr & e = p.getEvent(i);
            std::cout << " " << e->getDescription() << " " << e->getPrice() << std::endl;
        }
    }
}
