/*
 * Copyright (C) 2018 Peter Szabo
 *
 * This file is part of MoneyManager.
 *
 * MoneyManager is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MoneyManager is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MoneyManager.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <sstream>

#include "periodicevent.h"

using namespace mm;

PeriodicEvent::PeriodicEvent(std::string description, Money price,
                             std::array<bool, NUM_MONTHS> months, AccountType accountType)
    : Event(description, price, accountType)
{
    //    for (int i = 0; i < NUM_MONTHS; ++i)
    //        this->months[i] = months[i];
    this->months = months;
}

bool PeriodicEvent::isActiveThatYearMonth(YearMonth yearMonth)
{
    return months[yearMonth.month - 1];
}

std::string PeriodicEvent::save()
{
    std::stringstream ss;
    ss << "P ";
    for (const bool month : months)
    {
        ss << static_cast<int>(month) << " ";
    }
    ss << getPrice() << " " << static_cast<int>(getAccountType()) << " " << getDescription();

    return ss.str();
}

bool PeriodicEvent::load(std::string data)
{
    size_t pos;
    std::array<bool, NUM_MONTHS> months;
    for (int i = 0; i < NUM_MONTHS; ++i)
    {
        months[i] = std::stoi(data, &pos);
        ++pos;
        data = data.substr(pos);
    }

    int price = std::stoi(data, &pos);
    ++pos;

    data = data.substr(pos);
    int accountType = std::stoi(data, &pos);
    ++pos;

    std::string description = data.substr(pos);

    setMonths(months);
    setPrice(Money{price});
    setAccountType(static_cast<AccountType>(accountType));
    setDescription(description);

    return true; // TODO checks
}

void PeriodicEvent::finished()
{

}

std::array<bool, NUM_MONTHS> PeriodicEvent::getMonths() const
{
    return months;
}

void PeriodicEvent::setMonths(const std::array<bool, NUM_MONTHS> & value)
{
    months = value;
}
