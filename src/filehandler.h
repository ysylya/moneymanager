/*
 * Copyright (C) 2018 Peter Szabo
 *
 * This file is part of MoneyManager.
 *
 * MoneyManager is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MoneyManager is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MoneyManager.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef FILEHANDLER_H
#define FILEHANDLER_H

#include <string>
#include <fstream>

namespace mm
{
    class FileHandler
    {
        std::string path;

    public:
        virtual bool loadFromFile() = 0;
        virtual bool saveToFile() = 0;

        std::string getPath() const;
        void setPath(const std::string & value);

        bool openFile(std::fstream & fs, const std::string & name, bool append = true);

    };
}

#endif // FILEHANDLER_H
