/*
 * Copyright (C) 2018 Peter Szabo
 *
 * This file is part of MoneyManager.
 *
 * MoneyManager is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MoneyManager is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MoneyManager.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef EVENTOPERATOR_H
#define EVENTOPERATOR_H

#include <string>
#include <vector>

#include "filehandler.h"
#include "common.h"
#include "gui/emittingevent.h"

namespace mm
{
    class EventOperator : public FileHandler
    {
        EventShptrVec events;
        EventOperator() {}

    public:
        EventOperator(const EventOperator &) = delete;
        void operator=(const EventOperator &) = delete;

        static EventOperator & getInstance();

        bool loadFromFile() override;
        bool saveToFile() override;
        EventShptrVec & getEvents();
        void setEvents(const EventShptrVec & value);
    };
}

#endif // EVENTOPERATOR_H
