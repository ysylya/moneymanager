/*
 * Copyright (C) 2018 Peter Szabo
 *
 * This file is part of MoneyManager.
 *
 * MoneyManager is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MoneyManager is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MoneyManager.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "period.h"

using namespace mm;

Period::Period(YearMonth yearMonth)
    : yearMonth(yearMonth)
{}

void Period::addEvent(EventShptr & event)
{
    events.push_back(event);
}

EventShptr & Period::getEvent(size_t index)
{
    return events.at(index);
}

void Period::delEvent(size_t index)
{
    // TODO different if fixed or periodic
}

size_t Period::numEvents()
{
    return events.size();
}

YearMonth Period::getYearMonth() const
{
    return yearMonth;
}

void Period::setYearMonth(const YearMonth & value)
{
    yearMonth = value;
}

EventShptrVec & Period::getEvents()
{
    return events;
}

std::string Period::getName() const
{
    return std::to_string(yearMonth.year) + ". " + MONTHS[yearMonth.month];
}

//void Period::setEvents(const EventShptrVec & value)
//{
//    events = value;
//}
