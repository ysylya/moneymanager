/*
 * Copyright (C) 2018 Peter Szabo
 *
 * This file is part of MoneyManager.
 *
 * MoneyManager is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MoneyManager is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MoneyManager.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef CURRENTYEARMONTHLOADER_H
#define CURRENTYEARMONTHLOADER_H

#include "filehandler.h"
#include "yearmonth.h"

namespace mm
{
    class CurrentYearMonthLoader : public FileHandler
    {
        YearMonth currentYearMonth;

    public:
        CurrentYearMonthLoader();
        virtual bool loadFromFile() override;
        virtual bool saveToFile() override;
        YearMonth getCurrent() const;
        void setCurrent(const YearMonth & value);
    };
}

#endif // CURRENTYEARMONTHLOADER_H
