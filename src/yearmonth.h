/*
 * Copyright (C) 2018 Peter Szabo
 *
 * This file is part of MoneyManager.
 *
 * MoneyManager is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MoneyManager is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MoneyManager.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef YEARMONTH_H
#define YEARMONTH_H

#include <string>

namespace mm
{
    static const std::string MONTHS[] = {"None", "January", "February", "March", "April",
                                         "May", "June", "July", "August", "September",
                                         "October", "November", "December"};
    constexpr int NUM_MONTHS = 12;

    struct YearMonth
    {
        int year;
        int month;

        YearMonth(int year = 2018, int month = 7); // Birthmonth of this software

        bool operator==(const YearMonth & other) const;
        bool operator<(const YearMonth & other) const;
        bool operator<=(const YearMonth & other) const;
        bool operator>(const YearMonth & other) const;
        bool operator>=(const YearMonth & other) const;

        YearMonth operator+(const int n) const;
        int operator-(const YearMonth & other) const;
    };
}

#endif // YEARMONTH_H
