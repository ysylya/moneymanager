/*
 * Copyright (C) 2018 Peter Szabo
 *
 * This file is part of MoneyManager.
 *
 * MoneyManager is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MoneyManager is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MoneyManager.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <fstream>

#include "account.h"

using namespace mm;

Account::Account()
    : cash(0), bank(0), saving(0)
{
}

Account & Account::getInstance()
{
    static Account instance;
    return instance;
}

bool Account::loadFromFile()
{
    std::fstream fs;
    if (!openFile(fs, getPath()))
    {
        fs.close();
        return false;
    }

    std::string line;

    if (std::getline(fs, line))
        cash = std::stoi(line);

    if (std::getline(fs, line))
        bank = std::stoi(line);

    if (std::getline(fs, line))
        saving = std::stoi(line);

    fs.close();
    return true;
}

bool Account::saveToFile()
{
    std::fstream fs;
    if (!openFile(fs, getPath(), false))
    {
        fs.close();
        return false;
    }

    fs << cash << std::endl;
    fs << bank << std::endl;
    fs << saving << std::endl;

    fs.close();
    return true;
}

Money Account::getCash() const
{
    return cash;
}

void Account::setCash(const Money & value)
{
    cash = value;
}

Money Account::getBank() const
{
    return bank;
}

void Account::setBank(const Money & value)
{
    bank = value;
}

Money Account::getSaving() const
{
    return saving;
}

void Account::setSaving(const Money & value)
{
    saving = value;
}
