/*
 * Copyright (C) 2018 Peter Szabo
 *
 * This file is part of MoneyManager.
 *
 * MoneyManager is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MoneyManager is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MoneyManager.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef EVENT_H
#define EVENT_H

#include <string>

#include "common.h"
#include "yearmonth.h"
#include "account.h"

namespace mm
{
    class Event
    {
        std::string description;
        Money price;
        AccountType accountType;

    public:
        Event() = delete;
        Event(std::string description, Money price, AccountType accountType = AccountType::Cash);
        virtual ~Event() {}
        virtual bool isActiveThatYearMonth(YearMonth yearMonth) = 0;
        virtual std::string save() = 0;
        virtual bool load(std::string data) = 0; // TYPE WHENACTIVE PRICE DESCRIPTION
        virtual void finished() = 0;

        std::string getDescription() const;
        void setDescription(const std::string & value);
        Money getPrice() const;
        void setPrice(const Money & value);
        void changePriceBy(const Money & value);
        AccountType getAccountType() const;
        void setAccountType(const AccountType & value);
    };
}

#endif // EVENT_H
