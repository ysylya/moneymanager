/*
 * Copyright (C) 2018 Peter Szabo
 *
 * This file is part of MoneyManager.
 *
 * MoneyManager is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MoneyManager is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MoneyManager.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <fstream>
#include <memory>

#include "common.h"
#include "eventoperator.h"
#include "fixedevent.h"
#include "periodicevent.h"
#include "gui/emittingevent.h"

using namespace mm;

EventShptrVec & EventOperator::getEvents()
{
    return events;
}

void EventOperator::setEvents(const EventShptrVec & value)
{
    events = value;
}

EventOperator & EventOperator::getInstance()
{
    static EventOperator instance;
    return instance;
}

bool EventOperator::loadFromFile()
{
    std::fstream fs;
    if (!openFile(fs, getPath()))
    {
        fs.close();
        return false;
    }

    std::string line;
    while (std::getline(fs, line))
    {
        EventShptr e;
        std::unique_ptr<Event> event;
        switch (line[0])
        {
            case 'F':
                event = std::unique_ptr<FixedEvent>{new FixedEvent("", Money{0}, YearMonth{})};
                e = std::make_shared<gui::EmittingEvent>(std::move(event));
                break;
            case 'P':
                event = std::unique_ptr<PeriodicEvent>{new PeriodicEvent("", Money{0}, std::array<bool, 12>{{}})};
                e = std::make_shared<gui::EmittingEvent>(std::move(event));
                break;
            default:
                // a wrong line will be skipped
                continue;
        }
        e->load(line.substr(2));
        events.push_back(e);
    }

    fs.close();
    return true;
}

bool EventOperator::saveToFile()
{
    std::fstream fs;
    if (!openFile(fs, getPath(), false))
    {
        fs.close();
        return false;
    }

    for (EventShptr & e : events)
    {
        if (e->isEnabled())
        {
            fs << e->save() << std::endl;
        }
    }

    fs.close();
    return true;
}
