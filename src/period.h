/*
 * Copyright (C) 2018 Peter Szabo
 *
 * This file is part of MoneyManager.
 *
 * MoneyManager is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MoneyManager is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MoneyManager.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef PERIOD_H
#define PERIOD_H

#include <vector>
#include <string>
#include <memory>

#include "common.h"
#include "yearmonth.h"

namespace mm
{
    // for now periods are simply months,
    // this might be user-specified in the future, hence the classname
    class Period
    {
        YearMonth yearMonth;
        EventShptrVec events;
    public:
        Period(YearMonth yearMonth);
        void archive();
        void addEvent(EventShptr & event);
        EventShptr & getEvent(size_t index);
        void delEvent(size_t index);
        size_t numEvents();

        YearMonth getYearMonth() const;
        void setYearMonth(const YearMonth & value);
        EventShptrVec & getEvents(); // non-const!
//        void setEvents(const EventShptrVec & value);
        std::string getName() const;

    };
}

#endif // PERIOD_H
