/*
 * Copyright (C) 2018 Peter Szabo
 *
 * This file is part of MoneyManager.
 *
 * MoneyManager is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MoneyManager is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MoneyManager.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef COMMON_H
#define COMMON_H

#include <cstdint>
#include <vector>
#include <memory>
#include <iostream>

namespace mm
{
    // on some platforms int can be 16 bits which isn't enough,
    // so if I ever supported those, it would be easy to swap to a fixed-size integer
    using Money = int;

    namespace gui
    {
        class EmittingEvent;
    }
    using EventShptr = std::shared_ptr<mm::gui::EmittingEvent>;
    using EventShptrVec = std::vector<std::shared_ptr<mm::gui::EmittingEvent>>;

    template <class T>
    void cerr(T msg)
    {
        std::cerr << msg << std::endl;
    }
}

#endif // COMMON_H
