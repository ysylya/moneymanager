/*
 * Copyright (C) 2018 Peter Szabo
 *
 * This file is part of MoneyManager.
 *
 * MoneyManager is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MoneyManager is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MoneyManager.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "currentyearmonthloader.h"

using namespace mm;

CurrentYearMonthLoader::CurrentYearMonthLoader() // TODO default filepath
{
}

bool CurrentYearMonthLoader::loadFromFile()
{
    std::fstream fs;
    if (!openFile(fs, getPath()))
    {
        fs.close();
        return false;
    }

    std::string line;

    if (std::getline(fs, line) && line == "YM")
    {
        if (std::getline(fs, line))
        {
            currentYearMonth.year << std::stoi(line);
        }
        // TODO else throw

        if (std::getline(fs, line))
        {
            currentYearMonth.month << std::stoi(line);
        }

        return true;
    }
    else
    {
        // first start
        // TODO getting these things from the user
        currentYearMonth.year  = 2018;
        currentYearMonth.month = 9;
    }

    return false;
}

bool CurrentYearMonthLoader::saveToFile()
{
    std::fstream fs;
    if (!openFile(fs, getPath(), false))
    {
        fs.close();
        return false;
    }

    fs << "YM" << std::endl;
    fs << currentYearMonth.year << std::endl;
    fs << currentYearMonth.month << std::endl;

    fs.close();
    return true;
}

YearMonth CurrentYearMonthLoader::getCurrent() const
{
    return currentYearMonth;
}

void CurrentYearMonthLoader::setCurrent(const YearMonth & value)
{
    currentYearMonth = value;
}
