/*
 * Copyright (C) 2018 Peter Szabo
 *
 * This file is part of MoneyManager.
 *
 * MoneyManager is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MoneyManager is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MoneyManager.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <sstream>

#include "fixedevent.h"

using namespace mm;

FixedEvent::FixedEvent(std::string description, Money price, YearMonth yearMonth,
                       AccountType accountType, int duration)
    : Event(description, price,accountType), yearMonth(yearMonth), duration(duration)
{}

bool FixedEvent::isActiveThatYearMonth(YearMonth yearMonth)
{
    YearMonth end = this->yearMonth + duration;
    if (yearMonth >= this->yearMonth && yearMonth <= end)
        return true;
    else
        return false;
}

std::string FixedEvent::save()
{
    std::stringstream ss;
    ss << "F " << getYearMonth().year << " " << getYearMonth().month << " " << getDuration()
       << " " << getPrice() << " " << static_cast<int>(getAccountType()) << " " << getDescription();

    return ss.str();
}

bool FixedEvent::load(std::string data)
{
    size_t pos;

    int year = std::stoi(data, &pos);
    ++pos;

    data = data.substr(pos);
    int month = std::stoi(data, &pos);
    ++pos;

    data = data.substr(pos);
    int duration = std::stoi(data, &pos);
    ++pos;

    data = data.substr(pos);
    int price = std::stoi(data, &pos);
    ++pos;

    data = data.substr(pos);
    int accountType = std::stoi(data, &pos);
    ++pos;

    std::string description = data.substr(pos);

    setYearMonth({year, month});
    setDuration(duration);
    setPrice(Money{price});
    setAccountType(static_cast<AccountType>(accountType));
    setDescription(description);

    return true; // TODO checks
}

void FixedEvent::finished()
{

}

YearMonth FixedEvent::getYearMonth() const
{
    return yearMonth;
}

void FixedEvent::setYearMonth(const YearMonth & value)
{
    yearMonth = value;
}

int FixedEvent::getDuration() const
{
    return duration;
}

void FixedEvent::setDuration(int value)
{
    duration = value;
}

