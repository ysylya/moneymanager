/*
 * Copyright (C) 2018 Peter Szabo
 *
 * This file is part of MoneyManager.
 *
 * MoneyManager is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MoneyManager is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MoneyManager.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef FIXEDEVENT_H
#define FIXEDEVENT_H

#include <string>

#include "event.h"
#include "yearmonth.h"

namespace mm
{
    class FixedEvent : public Event
    {
        YearMonth yearMonth;
        int duration;

    public:
        FixedEvent() = delete;
        FixedEvent(std::string description, Money price, YearMonth yearMonth,
                   AccountType accountType = AccountType::Cash, int duration = 0);
        virtual bool isActiveThatYearMonth(YearMonth yearMonth) override;
        virtual std::string save() override;
        virtual bool load(std::string data) override;
        virtual void finished() override;

        YearMonth getYearMonth() const;
        void setYearMonth(const YearMonth & value);
        int getDuration() const;
        void setDuration(int value);

    };
}

#endif // FIXEDEVENT_H
