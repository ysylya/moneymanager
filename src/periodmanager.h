/*
 * Copyright (C) 2018 Peter Szabo
 *
 * This file is part of MoneyManager.
 *
 * MoneyManager is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MoneyManager is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MoneyManager.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef PERIODMANAGER_H
#define PERIODMANAGER_H

#include <vector>

#include "common.h"
#include "period.h"
#include "yearmonth.h"
#include "gui/emittingevent.h"

namespace mm
{
    class PeriodManager
    {
        YearMonth currentYM;
        size_t maxPeriodsToShow;
        EventShptrVec & events;
        std::vector<Period> periods;

    public:
        PeriodManager(YearMonth currentYM, size_t maxPeriodsToShow, EventShptrVec & events);
        PeriodManager(const PeriodManager & other);
        Period & operator[](size_t i);
        Period & at(size_t i);
        size_t getMaxPeriodsToShow() const;
        void setMaxPeriodsToShow(int value);
        void addNewEvent(EventShptr & event);
        void printEvents();

    private:
        void addExistingEvent(EventShptr & event);

    };
}

#endif // PERIODMANAGER_H
