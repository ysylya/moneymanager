/*
 * Copyright (C) 2018 Peter Szabo
 *
 * This file is part of MoneyManager.
 *
 * MoneyManager is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MoneyManager is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MoneyManager.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef ACCOUNT_H
#define ACCOUNT_H

#include <string>

#include "filehandler.h"
#include "common.h"

namespace mm
{
    enum class AccountType
    {
        Cash, Bank, Savings
    };

    class Account : public FileHandler
    {
        Money cash, bank, saving;
        Account();

    public:
        Account(const Account &) = delete;
        void operator=(const Account &) = delete;

        static Account & getInstance();

        bool loadFromFile() override;
        bool saveToFile() override;

        Money getCash() const;
        void setCash(const Money & value);
        Money getBank() const;
        void setBank(const Money & value);
        Money getSaving() const;
        void setSaving(const Money & value);
    };
}

#endif // ACCOUNT_H
