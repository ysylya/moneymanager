/*
 * Copyright (C) 2018 Peter Szabo
 *
 * This file is part of MoneyManager.
 *
 * MoneyManager is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MoneyManager is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MoneyManager.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "event.h"

using namespace mm;

Event::Event(std::string description, Money price, AccountType accountType)
    : description(description), price(price), accountType(accountType)
{}

std::string Event::getDescription() const
{
    return description;
}

void Event::setDescription(const std::string & value)
{
    description = value;
}

Money Event::getPrice() const
{
    return price;
}

void Event::setPrice(const Money & value)
{
    price = value;
}

void Event::changePriceBy(const Money & value)
{
    price += value;
}

AccountType Event::getAccountType() const
{
    return accountType;
}

void Event::setAccountType(const AccountType & value)
{
    // TODO could come from integer cast, maybe worth a check
    accountType = value;
}
