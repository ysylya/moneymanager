/*
 * Copyright (C) 2018 Peter Szabo
 *
 * This file is part of MoneyManager.
 *
 * MoneyManager is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MoneyManager is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MoneyManager.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <QApplication>
#include <QDesktopWidget>

#include "src/gui/mainwindow.h"

int main(int argc, char** argv)
{
    QApplication a(argc, argv);

    QDesktopWidget * desktop = QApplication::desktop();

    mm::gui::MainWindow mw{24};

    int posw = desktop->width() / 2 - mw.width() / 2;
    int posh = desktop->height() / 2 - mw.height() / 2;
    mw.move(posw, posh);
    mw.show();

    return a.exec();
}
